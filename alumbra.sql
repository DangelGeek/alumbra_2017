-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-04-2017 a las 15:33:57
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `alumbra`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasifica_persona`
--

CREATE TABLE IF NOT EXISTS `clasifica_persona` (
`id_clasf` int(11) NOT NULL,
  `desc` enum('pastor','lider','coordinador','miembro','invitado','contacto') NOT NULL DEFAULT 'miembro',
  `id_persona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE IF NOT EXISTS `evento` (
`id_evento` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `dire` varchar(111) COLLATE utf8_spanish2_ci NOT NULL,
  `id_iglesia` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`id_evento`, `nombre`, `fecha_ini`, `fecha_fin`, `dire`, `id_iglesia`) VALUES
(1, 'alumbra', '2017-04-12', '2017-04-16', 'la azulita, estado merida', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento_coord`
--

CREATE TABLE IF NOT EXISTS `evento_coord` (
  `cod_evento` int(11) NOT NULL,
  `cod_persona` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iglesia`
--

CREATE TABLE IF NOT EXISTS `iglesia` (
`id_iglesia` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` text COLLATE utf8_spanish_ci NOT NULL,
  `tlf` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `web` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla para las diferentes iglesias que participen en la convencion';

--
-- Volcado de datos para la tabla `iglesia`
--

INSERT INTO `iglesia` (`id_iglesia`, `nombre`, `direccion`, `tlf`, `web`) VALUES
(1, 'fuego de avivamiento', '', '', ''),
(2, 'luz de los robles', '', '', ''),
(3, 'la nueva jerusalen', '', '', ''),
(4, 'fe en accion', '', '', ''),
(5, 'la cruz', '', '', ''),
(6, 'amor y fe norte', '', '', ''),
(7, 'life church', '', '', ''),
(8, 'bethel-siloe', '', '', ''),
(9, 'c.c.c roca viva', '', '', ''),
(10, 'jehova gire', '', '', ''),
(11, 'santidad de Dios', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invitados_evento`
--

CREATE TABLE IF NOT EXISTS `invitados_evento` (
  `cod_evento` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `cedula` int(11) NOT NULL,
  `iglesia` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `ministerio` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `tlf` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ministerio`
--

CREATE TABLE IF NOT EXISTS `ministerio` (
`id_ministerio` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `cod_iglesia` int(11) NOT NULL,
  `dire` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `web` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `contacto` varchar(25) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ministerio`
--

INSERT INTO `ministerio` (`id_ministerio`, `nombre`, `cod_iglesia`, `dire`, `web`, `contacto`) VALUES
(17, 'rescate', 5, '', '', ''),
(18, 'revive', 5, '', '', ''),
(21, 'rec', 5, '', '', ''),
(22, 'renuevo', 5, '', '', ''),
(25, 'romanos 1:16', 5, '', '', ''),
(26, 'la luz en luz', 5, '', '', ''),
(27, 'la ley es cristo', 5, '', '', ''),
(28, 'el camino', 5, '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ministerio_detalle`
--

CREATE TABLE IF NOT EXISTS `ministerio_detalle` (
  `id_ministerio` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `tipo_persona` enum('lider','coordinador') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'coordinador'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ministerio_detalle`
--

INSERT INTO `ministerio_detalle` (`id_ministerio`, `id_persona`, `tipo_persona`) VALUES
(21, 2, 'lider');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participantes`
--

CREATE TABLE IF NOT EXISTS `participantes` (
`id_persona` int(11) NOT NULL,
  `cod_planilla` int(5) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(2) NOT NULL,
  `tlf` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `dire` text COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_iglesia` int(11) NOT NULL,
  `id_ministerio` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='pastores de las iglesias participantes en la convencion';

--
-- Volcado de datos para la tabla `participantes`
--

INSERT INTO `participantes` (`id_persona`, `cod_planilla`, `cedula`, `nombre`, `apellido`, `edad`, `tlf`, `dire`, `correo`, `id_iglesia`, `id_ministerio`) VALUES
(2, 101, 25041454, 'beatriz patricia', 'freite gamez', 21, '04246596007', 'zulia/maracaibo', '', 1, 17),
(3, 102, 25988608, 'josue david', 'diaz sanches', 21, '04166685573', 'zulia/san francisco', '', 2, 17),
(4, 103, 28545681, 'jesus esteban', 'montero vargas', 19, '04146728129', 'zulia/maracaibo', '', 3, 17),
(5, 104, 22474780, 'cesar alejandro', 'vilchez gonzales', 22, '04141674798', 'zulia/maracaibo', '', 4, 17),
(6, 105, 24362441, 'eliu jose', 'hernandez mendez', 22, '04267226937', 'zulia/san francisco', '3liuhernandez@gmail.com', 5, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universidad`
--

CREATE TABLE IF NOT EXISTS `universidad` (
`id_universidad` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `dire` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `contacto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clasifica_persona`
--
ALTER TABLE `clasifica_persona`
 ADD PRIMARY KEY (`id_clasf`), ADD UNIQUE KEY `desc` (`desc`), ADD UNIQUE KEY `desc_2` (`desc`,`id_persona`), ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
 ADD PRIMARY KEY (`id_evento`), ADD UNIQUE KEY `nombre` (`nombre`), ADD KEY `cod_lider` (`id_iglesia`), ADD KEY `id_iglesia` (`id_iglesia`);

--
-- Indices de la tabla `evento_coord`
--
ALTER TABLE `evento_coord`
 ADD UNIQUE KEY `cod_evento_2` (`cod_evento`,`cod_persona`), ADD KEY `cod_evento` (`cod_evento`), ADD KEY `cod_persona` (`cod_persona`);

--
-- Indices de la tabla `iglesia`
--
ALTER TABLE `iglesia`
 ADD PRIMARY KEY (`id_iglesia`), ADD UNIQUE KEY `nombre` (`nombre`,`tlf`);

--
-- Indices de la tabla `invitados_evento`
--
ALTER TABLE `invitados_evento`
 ADD UNIQUE KEY `nombre` (`nombre`,`cedula`,`correo`), ADD UNIQUE KEY `cod_evento` (`cod_evento`,`cedula`), ADD KEY `cod_evento_2` (`cod_evento`);

--
-- Indices de la tabla `ministerio`
--
ALTER TABLE `ministerio`
 ADD PRIMARY KEY (`id_ministerio`), ADD UNIQUE KEY `nombre` (`nombre`,`web`), ADD KEY `cod_iglesia` (`cod_iglesia`);

--
-- Indices de la tabla `ministerio_detalle`
--
ALTER TABLE `ministerio_detalle`
 ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `participantes`
--
ALTER TABLE `participantes`
 ADD PRIMARY KEY (`id_persona`), ADD UNIQUE KEY `tlf` (`tlf`,`correo`), ADD UNIQUE KEY `cedula` (`cedula`), ADD UNIQUE KEY `cod_planilla` (`cod_planilla`), ADD KEY `id_iglesia` (`id_iglesia`), ADD KEY `id_ministerio` (`id_ministerio`);

--
-- Indices de la tabla `universidad`
--
ALTER TABLE `universidad`
 ADD PRIMARY KEY (`id_universidad`), ADD UNIQUE KEY `nombre` (`nombre`), ADD KEY `contacto` (`contacto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clasifica_persona`
--
ALTER TABLE `clasifica_persona`
MODIFY `id_clasf` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `evento`
--
ALTER TABLE `evento`
MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `iglesia`
--
ALTER TABLE `iglesia`
MODIFY `id_iglesia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `ministerio`
--
ALTER TABLE `ministerio`
MODIFY `id_ministerio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `participantes`
--
ALTER TABLE `participantes`
MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `universidad`
--
ALTER TABLE `universidad`
MODIFY `id_universidad` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `evento`
--
ALTER TABLE `evento`
ADD CONSTRAINT `evento_ibfk_1` FOREIGN KEY (`id_iglesia`) REFERENCES `iglesia` (`id_iglesia`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `ministerio`
--
ALTER TABLE `ministerio`
ADD CONSTRAINT `ministerio_ibfk_1` FOREIGN KEY (`cod_iglesia`) REFERENCES `iglesia` (`id_iglesia`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `participantes`
--
ALTER TABLE `participantes`
ADD CONSTRAINT `participantes_ibfk_1` FOREIGN KEY (`id_iglesia`) REFERENCES `iglesia` (`id_iglesia`) ON UPDATE CASCADE,
ADD CONSTRAINT `participantes_ibfk_2` FOREIGN KEY (`id_ministerio`) REFERENCES `ministerio` (`id_ministerio`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
