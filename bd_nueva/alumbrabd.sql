-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 11, 2017 at 08:05 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alumbrabd`
--

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(7) NOT NULL,
  `ckeck` int(7) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `ckeck`, `nombre`, `apellido`) VALUES
(1, 1, 'Abril', 'Álvarez'),
(2, 2, 'Alfonsina', 'Parra'),
(3, 3, 'Ana', 'Sarjeant'),
(4, 4, 'Anais', 'Hernandez'),
(5, 5, 'Andrea', 'Romero'),
(6, 6, 'Andresa', 'Zabaleta'),
(7, 7, 'Anyelina', 'Rubio'),
(8, 8, 'Artyelit', 'Hernandez'),
(9, 9, 'Ashley', 'Reyes'),
(10, 10, 'Beatriz', 'Freites'),
(11, 11, 'Carol', 'Romero'),
(12, 12, 'Cielo ', 'Rada'),
(13, 13, 'Claudia', 'Chacin'),
(14, 14, 'Crizelda', 'Bellarline'),
(15, 15, 'Dayerlin', 'Julio'),
(16, 16, 'Deisy', 'Reyes'),
(17, 17, 'Edianna', 'Prieto'),
(18, 18, 'Eidimar', 'Niño'),
(19, 19, 'Elimar', 'Oberto'),
(20, 20, 'Erimeck', 'Chaparro'),
(21, 21, 'Lila', 'De Martinez'),
(22, 22, 'Eudice', 'Garrillo'),
(23, 23, 'Eunice', 'Quintero'),
(24, 24, 'Fabiola', 'Castillo'),
(25, 25, 'Genesis', 'Teran'),
(26, 26, 'Hennit', 'Gallardo'),
(27, 27, 'Iris', 'De Velasquez'),
(28, 28, 'Jayne', 'Lopez'),
(29, 29, 'Juannys', 'Rincon');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `ckeck` (`ckeck`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
