<?php 
	require_once('../plugins/fpdf/fpdf.php');
	include_once('../modelos/listar.php');
	
	class PDF extends FPDF //heredo la clase pdf
	{
	// Cabecera de página
	function Header()
		{
		    $this->Ln(10);//salto de linea
		    // Logo
		    $this->Image('../plugins/logo.png',10,10,20); //agrego la imagen
		    // Arial bold 15
		    $this->SetFont('Arial','B',15); //tipo de letra
		    //definir color de fuente RGB
		    //$this->setTextColor(150, 10, 250);
		    // Movernos a la derecha
		    //$this->Cell(80);
		    // Título
		    $this->Cell(85,10,'Listado de Clientes con mascotas',0,0,'R');

		    $this->SetFont('Arial','B',16); //tipo de letra
		    $this->setTextColor(150, 100, 150);

		    $this->Cell(85,10,'Fecha: '.date('d-m-y'),0,1,'R'); //fecha dia, mes, año
		    // Salto de línea
		    $this->Ln(10);
		}

	// Pie de página
	function Footer()
		{
			$this->SetTextColor(0);
		    // Posición: a 1,5 cm del final
		    $this->SetY(15); //en el eje Y (vertical), abajo
		    // Arial italic 8
		    $this->SetFont('Arial','I',8);
		    // Número de página
		    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
		}
	}

// Creación del objeto de la clase heredada
	$respuesta = listar::listado(); //llamada a la clase listar.php con la consulta de los datos a mostrar
	$pdf = new PDF('P', 'mm', 'A4'); //hoja vertical en milimetros, con tipo de hoja
	//$pdf = new PDF('L', 'mm', [270, 10]); //hoja horizontal tamaño especifico en milimitros, ancho 270, alto 10 
	$pdf->SetMargins(10,30);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','I',26);
	$count = 1;

	$pdf->Cell(35,8,'',0,0, 'C');
	$pdf->Cell(125,12,'Resultado de la busqueda',1,1, 'C');
	$pdf->SetFont('Times','',12);
	$pdf->Cell(35,8,'',0,0, 'C');
	$pdf->Cell(15,8,'Numero',1,0, 'C');
	$pdf->Cell(25,8,'Nombre',1,0);
	$pdf->Cell(25,8,'Apellido',1,0);
	$pdf->Cell(25,8,'Telefono',1,0);
	$pdf->Cell(35,8,'nombre mascota',1,1);

	//ejecuto un bucle para mostrar los datos
	while($resp = mysqli_fetch_array($respuesta)){
			$pdf->Cell(35,8,'',0,0);
			$pdf->Cell(15,8,$count,1,0);
			$pdf->Cell(25,8,$resp['nombre'],1,0);
			$pdf->Cell(25,8,$resp['Apellido'],1,0);
			$pdf->Cell(25,8,$resp['telefono'],1,0);
			$pdf->Cell(35,8,$resp['nombremascota'],1,1);
			$count++;
		}
	$pdf->Output();


 ?>