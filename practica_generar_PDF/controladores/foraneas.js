;
$(document).ready(function(){
	$.ajax({
		url: '../modelos/raza.php',
		success:function(resp){
			resp = $.parseJSON(resp);
			for (var i = 0; i < resp.length; i++) {
				$('#raza').append("<option value="+resp[i].id+">"+resp[i].nombre+"</option>");
			}
		}
	});
});

$(document).ready(function () {
	$('#guardar').click(function (event) {
		event.preventDefault();

		var datos = {
			nombre: $('#nombre').val(),
			apellido: $('#apellido').val(),
			telefono: $('#telefono').val(),
			raza: $('#raza').val(),
			nombremascota: $('#nombremascota').val(),
		}

		$.ajax({
			url: '../modelos/foraneas.php',
			type: 'POST',
			data: datos,
			success:function(resp){
				resp = $.parseJSON(resp);
				$('#mensaje').html(resp.Mensaje).addClass(resp.tipo).fadeIn(500);

			}
		});
	})
});


