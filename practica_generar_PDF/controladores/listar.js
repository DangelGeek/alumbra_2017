;

$(document).on("ready", tabla());

/*=========================================================
=            Se ejecuta cuando carga la pagina            =
=========================================================*/
function tabla() {
	$('#listado').empty();
	var dato = {
		dato:1,
	}
	$.ajax({
		url: '../modelos/listar.php',
		data:dato,
		success:function(resp){
			resp = $.parseJSON(resp);
			for (var i = 0; i < resp.length; i++) {
				$('#listado').append("<tr><td>"+
					resp[i].nombre+"</td><td>"+resp[i].apellido+
					"</td><td>"+resp[i].telefono+"</td><td>"+resp[i].nombremascota+
					"</td><td><button class='btn btn-default' value="+resp[i].id+">X</button></td>"+
					"<td><button class='btn btn-warning actualizar' value="+resp[i].id+">Actualizar</button></td></tr>");
			}
		},
	})
		.done(function() {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			$('.btn-default').click(function () {
				borrar($(this).val());
			});
			$('.actualizar').click(function () {
				buscarActualizar($(this).val());
			});
		console.log("complete");
		});

}
/*=======================================================================================
=            Se ejecuta cuando se aprieta el boton de actualizar en la lista            =
=======================================================================================*/
function buscarActualizar(id) {
	var datos = {
		id:id
	}
	$.ajax({
		url: '../modelos/buscarActualizar.php',
		data: datos,
		success:function (argument) {
			resp = $.parseJSON(argument);
			$('#usuarioID').val(resp.id);
			$('#actNombre').val(resp.nombre);
			$('#actApellido').val(resp.apellido);
			$('#actTelefono').val(resp.telefono);
			$('#actMascotas').val(resp.nombremascota);
			$('#actualizacion').fadeIn(500);
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}
/*===========================================================================
=            Se ejecuta cuando se aprieta el boton en actualizar            =
===========================================================================*/
function actualizacion() {
	var datos = {
		id: $('#usuarioID').val(),
		nombre: $('#actNombre').val(),
		apellido: $('#actApellido').val(),
		telefono:$('#actTelefono').val(),
		mascota: $('#actMascotas').val(),
	}
	$.ajax({
		url: '../modelos/actualizar.php',
		data: datos,
		success:function (argument) {
			tabla();
			$('#mensaje').html('Se actualizaron correctamente los datos').addClass('alert-success').fadeIn(500);
			$('#actualizacion').fadeOut(500);
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

}
/*=========================================================
=            Se ejecuta cuando se aprieta la X            =
=========================================================*/
function borrar(id) {
	var datos = {
		id:id
	}
	$.ajax({
		url: '../modelos/borrar.php',
		data: datos,
		success:function () {
			tabla();
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}
