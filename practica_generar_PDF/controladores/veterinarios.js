;
$(document).ready(function(){
	$.ajax({
		url: '../modelos/usuarios.php',
		success:function(resp){
			resp = $.parseJSON(resp);
			for (var i = 0; i < resp.length; i++) {
				$('#usuarios_id').append("<option value="+resp[i].id+">"+resp[i].nombre+"</option>");
			}
		}
	});
});

$(document).ready(function () {
	$('#guardar').click(function (event) {
		event.preventDefault();

		var datos = {
			nombre: $('#nombre').val(),
			apellido: $('#apellido').val(),
			colegio: $('#colegio').val(),
			usuarios_id: $('#usuarios_id').val(),
		}

		$.ajax({
			url: '../modelos/veterinario.php',
			type: 'POST',
			data: datos,
			success:function(resp){
				resp = $.parseJSON(resp);

				$('#mensaje').html(resp.Mensaje).addClass(resp.tipo).fadeIn(500);

			}
		});
	})
})
