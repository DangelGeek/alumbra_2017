;
$(document).ready(function () {
	$('#guardar').click(function (event) {
		event.preventDefault();

		var datos = {
			nombre: $('#nombre').val(),
			peso: $('#peso').val(),
			altura: $('#altura').val(),
		}

		$.ajax({
			url: '../modelos/grabar.php',
			type: 'POST',
			data: datos,
			success:function(resp){
				resp = $.parseJSON(resp);
				
				$('#mensaje').html(resp.Mensaje).addClass(resp.tipo).fadeIn(500);

			}
		});
	})
})
	