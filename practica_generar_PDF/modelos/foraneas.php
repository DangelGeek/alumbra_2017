<?php 
	include_once('../conexion/conexion.php');
	$con = new conectar();

	$nombre = $_REQUEST['nombre'];
	$apellido = $_REQUEST['apellido'];
	$telefono = $_REQUEST['telefono'];
	$raza = $_REQUEST['raza'];
	$nombremascota = $_REQUEST['nombremascota'];
	$historia = time();

	/*===================================================
	=            Primero se graba el Usuario            =
	===================================================*/
	
	$sSql = "INSERT INTO `tercera`.`usuarios` (`nombre`, `Apellido`, `telefono`) VALUES ('$nombre', '$apellido ', '$telefono');";

	if(!mysqli_query($con->conectarse(), $sSql)){
		$resp['tipo'] = 'alert-danger';
		$resp['Mensaje'] = 'error al registrar el usuario';

		$resp = json_encode($resp);
		echo $resp;

	}else{ 

		/*=============================================================================
		=            Se seleccionna el registro que se acaba de introducir            =
		=============================================================================*/
	
		$sSqlId = "SELECT MAX(id) as `id` FROM `tercera`.`usuarios`";
		//practica recomendada para devolver el ultimo id
		//http://php.net/manual/es/function.mysql-insert-id.php
		if($respuesta = mysqli_query($con->conectarse(), $sSqlId)){
			foreach ($respuesta as $resp) {
				$id = $resp['id'];
			}
		}
		
		/*========================================================
		=            Se graba en la tabla de mascotas            =
		========================================================*/
		
		$sSqlMas = "INSERT INTO `tercera`.`mascotas` (`razas_id`, `usuarios_id`, `historiaMedica`, `nombremascota`) VALUES ('$raza', '$id', '$historia', '$nombremascota')";

		if (mysqli_query($con->conectarse(), $sSqlMas)) {
			$resp['tipo'] = 'alert-success';
			$resp['Mensaje'] = 'Se ha registrado correctamente el Usuario y la mascota';
		} else {
			$resp['tipo'] = 'alert-danger';
			$resp['Mensaje'] = 'error al registrar la mascota';
		}
		
	}
	
	mysqli_close($con->conectarse());

	$resp = json_encode($resp);

	echo $resp;
 ?>