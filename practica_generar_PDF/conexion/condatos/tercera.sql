-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-04-2017 a las 01:56:33
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tercera`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultas`
--

CREATE TABLE IF NOT EXISTS `consultas` (
`id` int(11) NOT NULL,
  `mascotas_id` int(11) NOT NULL,
  `veterinarios_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidades`
--

CREATE TABLE IF NOT EXISTS `especialidades` (
`id` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `veterinarios_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascotas`
--

CREATE TABLE IF NOT EXISTS `mascotas` (
`id` int(11) NOT NULL,
  `razas_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `historiaMedica` varchar(45) DEFAULT NULL,
  `nombremascota` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `mascotas`
--

INSERT INTO `mascotas` (`id`, `razas_id`, `usuarios_id`, `historiaMedica`, `nombremascota`) VALUES
(1, 2, 1, '1493412904', 'Benju'),
(2, 1, 2, '1493412930', 'Olivert'),
(3, 1, 3, '1493412991', 'Olivert'),
(4, 2, 4, '1493413028', 'Benji');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `razas`
--

CREATE TABLE IF NOT EXISTS `razas` (
`id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `altura` varchar(45) DEFAULT NULL,
  `peso` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `razas`
--

INSERT INTO `razas` (`id`, `nombre`, `altura`, `peso`) VALUES
(1, 'Golden', '70 cms', '60 kg'),
(2, 'Labrador', '60 cms', '50 kg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `Apellido`, `telefono`) VALUES
(1, 'Daniel', 'Angel ', '04246504181'),
(2, 'Ezequiel', 'Angel ', '02617528123'),
(3, 'Ramon', 'Perez ', '04241234123'),
(4, 'Elias', 'Angel ', '04246504181');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `veterinarios`
--

CREATE TABLE IF NOT EXISTS `veterinarios` (
`id` int(11) NOT NULL,
  `colegio` varchar(45) DEFAULT NULL,
  `usuarios_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `veterinarios`
--

INSERT INTO `veterinarios` (`id`, `colegio`, `usuarios_id`, `nombre`, `apellido`) VALUES
(1, 'San Juan', 1, 'Jose', 'Almirante');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `consultas`
--
ALTER TABLE `consultas`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_consultas_mascotas1_idx` (`mascotas_id`), ADD KEY `fk_consultas_veterinarios1_idx` (`veterinarios_id`);

--
-- Indices de la tabla `especialidades`
--
ALTER TABLE `especialidades`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_especialidades_veterinarios1_idx` (`veterinarios_id`);

--
-- Indices de la tabla `mascotas`
--
ALTER TABLE `mascotas`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_mascotas_razas1_idx` (`razas_id`), ADD KEY `fk_mascotas_usuarios1_idx` (`usuarios_id`);

--
-- Indices de la tabla `razas`
--
ALTER TABLE `razas`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `veterinarios`
--
ALTER TABLE `veterinarios`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_veterinarios_usuarios_idx` (`usuarios_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `consultas`
--
ALTER TABLE `consultas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `especialidades`
--
ALTER TABLE `especialidades`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mascotas`
--
ALTER TABLE `mascotas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `razas`
--
ALTER TABLE `razas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `veterinarios`
--
ALTER TABLE `veterinarios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `consultas`
--
ALTER TABLE `consultas`
ADD CONSTRAINT `fk_consultas_mascotas1` FOREIGN KEY (`mascotas_id`) REFERENCES `mascotas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_consultas_veterinarios1` FOREIGN KEY (`veterinarios_id`) REFERENCES `veterinarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `especialidades`
--
ALTER TABLE `especialidades`
ADD CONSTRAINT `fk_especialidades_veterinarios1` FOREIGN KEY (`veterinarios_id`) REFERENCES `veterinarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mascotas`
--
ALTER TABLE `mascotas`
ADD CONSTRAINT `fk_mascotas_razas1` FOREIGN KEY (`razas_id`) REFERENCES `razas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_mascotas_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `veterinarios`
--
ALTER TABLE `veterinarios`
ADD CONSTRAINT `fk_veterinarios_usuarios` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
