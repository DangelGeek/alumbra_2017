-- MySQL Script generated by MySQL Workbench
-- vie 31 mar 2017 15:15:38 VET
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tercera
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `tercera` ;

-- -----------------------------------------------------
-- Schema tercera
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tercera` DEFAULT CHARACTER SET utf8 ;
USE `tercera` ;

-- -----------------------------------------------------
-- Table `tercera`.`razas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tercera`.`razas` ;

CREATE TABLE IF NOT EXISTS `tercera`.`razas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `altura` VARCHAR(45) NULL DEFAULT NULL,
  `peso` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tercera`.`usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tercera`.`usuarios` ;

CREATE TABLE IF NOT EXISTS `tercera`.`usuarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `Apellido` VARCHAR(45) NULL DEFAULT NULL,
  `telefono` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tercera`.`mascotas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tercera`.`mascotas` ;

CREATE TABLE IF NOT EXISTS `tercera`.`mascotas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `razas_id` INT(11) NOT NULL,
  `usuarios_id` INT(11) NOT NULL,
  `historiaMedica` VARCHAR(45) NULL DEFAULT NULL,
  `nombremascota` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mascotas_razas1_idx` (`razas_id` ASC),
  INDEX `fk_mascotas_usuarios1_idx` (`usuarios_id` ASC),
  CONSTRAINT `fk_mascotas_razas1`
    FOREIGN KEY (`razas_id`)
    REFERENCES `tercera`.`razas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mascotas_usuarios1`
    FOREIGN KEY (`usuarios_id`)
    REFERENCES `tercera`.`usuarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tercera`.`veterinarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tercera`.`veterinarios` ;

CREATE TABLE IF NOT EXISTS `tercera`.`veterinarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `colegio` VARCHAR(45) NULL DEFAULT NULL,
  `usuarios_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_veterinarios_usuarios_idx` (`usuarios_id` ASC),
  CONSTRAINT `fk_veterinarios_usuarios`
    FOREIGN KEY (`usuarios_id`)
    REFERENCES `tercera`.`usuarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tercera`.`consultas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tercera`.`consultas` ;

CREATE TABLE IF NOT EXISTS `tercera`.`consultas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `mascotas_id` INT(11) NOT NULL,
  `veterinarios_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_consultas_mascotas1_idx` (`mascotas_id` ASC),
  INDEX `fk_consultas_veterinarios1_idx` (`veterinarios_id` ASC),
  CONSTRAINT `fk_consultas_mascotas1`
    FOREIGN KEY (`mascotas_id`)
    REFERENCES `tercera`.`mascotas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_consultas_veterinarios1`
    FOREIGN KEY (`veterinarios_id`)
    REFERENCES `tercera`.`veterinarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tercera`.`especialidades`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tercera`.`especialidades` ;

CREATE TABLE IF NOT EXISTS `tercera`.`especialidades` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL DEFAULT NULL,
  `veterinarios_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_especialidades_veterinarios1_idx` (`veterinarios_id` ASC),
  CONSTRAINT `fk_especialidades_veterinarios1`
    FOREIGN KEY (`veterinarios_id`)
    REFERENCES `tercera`.`veterinarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
