<?php
session_start();
unset($_COOKIE['recuerdaId']);
unset($_COOKIE['recuerdaUsuario']);
unset($_COOKIE['recuerdaJerarquia']);
unset($_COOKIE['recuerdaTipo']);

setcookie('recuerdaId', null, -1, '/');
setcookie('recuerdaUsuario', null, -1, '/');
setcookie('recuerdaJerarquia', null, -1, '/');
setcookie('recuerdaTipo', null, -1, '/');
// remove all session variables
session_unset();
// destroy the session
session_destroy(); 
header('Location: ../login/login.php');
?>